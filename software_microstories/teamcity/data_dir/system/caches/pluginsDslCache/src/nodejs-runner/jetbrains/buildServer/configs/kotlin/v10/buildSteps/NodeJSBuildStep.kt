package jetbrains.buildServer.configs.kotlin.v10.buildSteps

import jetbrains.buildServer.configs.kotlin.v10.*

/**
 * A Node.js build step
 * 
 * @see nodeJS
 */
open class NodeJSBuildStep : BuildStep {
    constructor(init: NodeJSBuildStep.() -> Unit = {}, base: NodeJSBuildStep? = null): super(base = base as BuildStep?) {
        type = "nodejs-runner"
        init()
    }

    /**
     * Shell commands for running Node.js projects
     */
    var shellScript by stringParameter()

    /**
     * Specifies which Docker image platform will be used to run this build step.
     */
    var dockerImagePlatform by enumParameter<ImagePlatform>("plugin.docker.imagePlatform", mapping = ImagePlatform.mapping)

    /**
     * If enabled, "docker pull [image][dockerImage]" will be run before docker run.
     */
    var dockerPull by booleanParameter("plugin.docker.pull.enabled", trueValue = "true", falseValue = "")

    /**
     * Specifies which Docker image to use for running this build step. I.e. the build step will be run inside specified docker image, using 'docker run' wrapper.
     */
    var dockerImage by stringParameter("plugin.docker.imageId")

    /**
     * Additional docker run command arguments
     */
    var dockerRunParameters by stringParameter("plugin.docker.run.parameters")

    /**
     * Docker image platforms
     */
    enum class ImagePlatform {
        Any,
        Linux,
        Windows;

        companion object {
            val mapping = mapOf<ImagePlatform, String>(Any to "", Linux to "linux", Windows to "windows")
        }

    }
}


/**
 * Adds a Node.js build step
 * @see NodeJSBuildStep
 */
fun BuildSteps.nodeJS(base: NodeJSBuildStep? = null, init: NodeJSBuildStep.() -> Unit = {}) {
    step(NodeJSBuildStep(init, base))
}
